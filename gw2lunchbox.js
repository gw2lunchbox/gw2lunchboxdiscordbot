const Discord = require('discord.js');
const bot = new Discord.Client();
const request = require('request');

bot.on('message', (message) => {
  if (message.content == '[]help') {
    message.author.send('[]help - gets all commands \n' +
    '[]ping - will respond to the user to confirm the bot is running \n' +
    '[]ttstest - sends a short test message to the server with TTS enabled \n' +
    '[]dhuumtimer start - starts the timer for dhuum, all responses will be sent with text and as TTS. for best results change the name of the bot on your sever to "." so that the TTS responds with " says: <actual response>"... having the full name of the bot repeated every time may not be ideal \n' +
    '[]dhuumtimer stop - stops the timer for dhuum \n' +
    '[]dhuumtimer setorb1 - set the name of "Orb 1" to whatever text you want to be read \n' +
    '[]dhuumtimer setorb2 - set the name of "Orb 2" to whatever text you want to be read \n' +
    '[]dhuumtimer setorb3 - set the name of "Orb 3" to whatever text you want to be read \n' +
    '[]dhuumtimer voice - toggles the Text-To-Speech feature of the dhuum timer');
  }
  if (message.content == '[]ping') {
    message.reply('yes?');
  }
  if (message.content == '[]dhuumtimer start') {
    cdstart(message.channel);
  }
  if (message.content == '[]dhuumtimer stop') {
    cdreset();
  }
  if (message.content.startsWith('[]dhuumtimer setorb1 ')) {
    orb1Val = message.content.substr(21);
  }
  if (message.content.startsWith('[]dhuumtimer setorb2 ')) {
    orb2Val = message.content.substr(21);
  }
  if (message.content.startsWith('[]dhuumtimer setorb3 ')) {
    orb3Val = message.content.substr(21);
  }
  if (message.content == '[]dhuumtimer voice') {
    playTTS = !playTTS;
    message.channel.send('Voice: ' + (voice ? 'ON' : 'OFF'));
  }
  if (message.content == '[]ttstest') {
    message.channel.send('this is a test', {tts: true});
  }
  if (message.content.startsWith('[]reclaimed')) {
    var reqsort = message.content.substr(11);
    if (reqsort == ' bd') {
      request(baseUrl + 'v2/items?ids=' + reclaimedWeaponsItemIds.toString(), function(error, response, body){
        prebuiltCompare(body, message.channel, true, true);
      });
    } else if (reqsort == ' ba') {
      request(baseUrl + 'v2/items?ids=' + reclaimedWeaponsItemIds.toString(), function(error, response, body){
        prebuiltCompare(body, message.channel, true, false);
      });
    } else if (reqsort == ' sd') {
      request(baseUrl + 'v2/items?ids=' + reclaimedWeaponsItemIds.toString(), function(error, response, body){
        prebuiltCompare(body, message.channel, false, true);
      });
    } else {
      request(baseUrl + 'v2/items?ids=' + reclaimedWeaponsItemIds.toString(), function(error, response, body){
        prebuiltCompare(body, message.channel, false, false);
      });
    }
  }
});

bot.login('<YOUR DISCORD BOT USER TOKEN GOES HERE>');

///////HELPER METHODS START
var baseUrl = 'https://api.guildwars2.com/';

var convertValueToGoldString = function (value) {
    var returnValue = 'N/A';

    if (value) {
        var amount = value.unit_price;

        var gold = '';
        var silver = '';
        var copper = '';

        if (amount > 0) {
            copper = amount % 100;
            amount = Math.floor(amount / 100);
        }
        if (amount > 0) {
            silver = amount % 100;
            amount = Math.floor(amount / 100);
        }
        if (amount > 0) {
            gold = amount
        }

        returnValue =
            (gold > 0 ? gold + 'g ' : '') +
            (silver > 0 ? (silver < 10 && gold > 0 ? '0' + silver : silver) + 's ' : (gold > 0 ? '00s ' : '')) +
            (copper > 0 ? (copper < 10 && silver > 0 ? '0' + copper : copper) + 'c' : (gold > 0 || silver > 0 ? '00c' : ''));
    }

    return returnValue;
};
///////HELPER METHODS END

///////DHUUMTIMER START
var CCOUNT = 10 * 60;//10 minutes
var orb1Val = 'Orb 1';
var orb2Val = 'Orb 2';
var orb3Val = 'Orb 3';
var t, count, lastText, channelToPlayTo;
var orb1 = orb2 = orb3 = orbwarning = orbup = sswarning = ssup = voice = true;
var chime = nosound = playTTS = false;

var displayData = [
    { time: 600, type: "", text: "" },
    { time: 580, type: "orbwarning,orb1", text: "{1} to Arrow" },
    { time: 570, type: "orbup,orb1", text: "{1} going up" },
    { time: 550, type: "orbwarning,orb2", text: "{2} to Circle" },
    { time: 540, type: "orbup,orb2", text: "{2} going up, bombs starting" },
    { time: 520, type: "orbwarning,orb3", text: "{3} to Heart" },
    { time: 510, type: "orbup,orb3", text: "{3} going up" },
    { time: 490, type: "orbwarning,orb1", text: "{1} to Square" },
    { time: 480, type: "orbup,orb1", text: "{1} going up, Boss is activated" },
    { time: 460, type: "orbwarning,orb2", text: "{2} to Star" },
    { time: 450, type: "orbup,orb2", text: "{2} going up" },
    { time: 430, type: "orbwarning,orb3", text: "{3} to Spiral" },
    { time: 420, type: "orbup,orb3", text: "{3} going up" },
    { time: 400, type: "orbwarning,orb1", text: "{1} to Triangle" },
    { time: 395, type: "sswarning", text: "prepare for soul slam" },
    { time: 390, type: "orbup,orb1", text: "{1} going up" },
    { time: 385, type: "ssup", text: "soul slam! now" },
    { time: 370, type: "orbwarning,orb2", text: "{2} to Arrow" },
    { time: 360, type: "orbup,orb2", text: "{2} going up" },
    { time: 340, type: "orbwarning,orb3", text: "{3} to Circle" },
    { time: 330, type: "orbup,orb3", text: "{3} going up" },
    { time: 315, type: "sswarning,orbwarning,orb1", text: "prepare for soul slam, {1} to Heart AFTER soul slam" },
    { time: 305, type: "ssup", text: "soul slam! now" },
    { time: 300, type: "orbup,orb1", text: "{1} going up" },
    { time: 280, type: "orbwarning,orb2", text: "{2} to Square" },
    { time: 270, type: "orbup,orb2", text: "{2} going up" },
    { time: 250, type: "orbwarning,orb3", text: "{3} to Star" },
    { time: 240, type: "orbup,orb3", text: "{3} going up" },
    { time: 235, type: "sswarning", text: "prepare for soul slam" },
    { time: 225, type: "ssup", text: "soul slam! now" },
    { time: 220, type: "orbwarning,orb1", text: "{1} to Spiral" },
    { time: 210, type: "orbup,orb1", text: "{1} going up" },
    { time: 190, type: "orbwarning,orb2", text: "{2} to Triangle" },
    { time: 180, type: "orbup,orb2", text: "{2} going up" },
    { time: 160, type: "orbwarning,orb3", text: "{3} to Arrow" },
    { time: 155, type: "sswarning", text: "prepare for soul slam" },
    { time: 150, type: "orbup,orb3", text: "{3} going up" },
    { time: 145, type: "ssup", text: "soul slam! now" },
    { time: 130, type: "orbwarning,orb1", text: "{1} to Circle" },
    { time: 120, type: "orbup,orb1", text: "{1} going up" },
    { time: 100, type: "orbwarning,orb2", text: "{2} to Heart" },
    { time: 90, type: "orbup,orb2", text: "{2} going up" },
    { time: 75, type: "sswarning,orbwarning,orb3", text: "prepare for soul slam, {3} to Square AFTER soul slam" },
    { time: 65, type: "ssup", text: "soul slam! now" },
    { time: 60, type: "orbup,orb3", text: "{3} going up, 1 minute until enrage" },
    { time: 40, type: "orbwarning,orb1", text: "{1} to Star" },
    { time: 30, type: "orbup,orb1", text: "{1} going up, 30 seconds until enrage" },
    { time: 10, type: "orbwarning,orb2", text: "{2} to Spiral" },
    { time: 0, type: "orbup,orb2", text: "{2} going up, you are in enrage" }
];

function cdstart(startChannel) {
  channelToPlayTo = startChannel;

    delay = true;

    clearTimeout(t);
    count = delay ? CCOUNT + 10 : CCOUNT;
    cddisplay();
    countdown();
}

function cddisplay(reset) {
    getDisplayText(count);
};

function countdown() {
    // starts countdown
    cddisplay();
    if (count == 0) {
        // time is up
        cdpause();
    } else {
        count--;
        t = setTimeout(countdown, 1000);
    }
};

function cdpause() {
    clearTimeout(t);
};

function cdreset() {
    cdpause();
    count = CCOUNT;
    //cddisplay(true);
};

var getDisplayText = function (seconds) {
    if (seconds >= 600 && delay) {

        if (seconds == 605) {
            channelToPlayTo.send('fight starting', {tts: playTTS});
            return "fight starting";
        } else if (seconds == 603) {
            channelToPlayTo.send('3', {tts: playTTS});
            return "3";
        } else if (seconds == 602) {
            channelToPlayTo.send('2', {tts: playTTS});
            return "2";
        } else if (seconds == 601) {
            channelToPlayTo.send('1', {tts: playTTS});
            return "1";
        } else if (seconds == 600) {
            channelToPlayTo.send('Go!', {tts: playTTS});
            return "Go!";
        }
    } else {

        for (var i = 0; i < displayData.length; i++) {
            if (displayData[i + 1] && displayData[i + 1].time < count) {
                var orb1check = orb1 && displayData[i].type.indexOf('orb1') >= 0;
                var orb2check = orb2 && displayData[i].type.indexOf('orb2') >= 0;
                var orb3check = orb3 && displayData[i].type.indexOf('orb3') >= 0;
                var ssCheck = ssup && (displayData[i].type.indexOf('sswarning') >= 0 || displayData[i].type.indexOf('ssup') >= 0);

                if (orb1check || orb2check || orb3check || ssCheck) {
                    if (lastText != displayData[i].text && seconds <= displayData[1].time) {
                        lastText = displayData[i].text;

                        if (voice) {
                            if (orb1check || orb2check || orb3check) {
                                channelToPlayTo.send(lastText.replace('{1}', orb1Val).replace('{2}', orb2Val).replace('{3}', orb3Val), {tts: playTTS});
                            } else {
                                channelToPlayTo.send(lastText, {tts: playTTS});
                            }
                        } else if (chime) {
                            if (orb1check || orb2check || orb3check) {
                                //singlebeep.play();
                            } else {
                                //goingdownzip.play();
                            }
                        }
                    }
                    return displayData[i].text.replace('{1}', orb1Val).replace('{2}', orb2Val).replace('{3}', orb3Val);
                } else {
                    return "";
                }
            }
        }
    }

    return "";
}
///////DHUUMTIMER END

///////RECLAIMED WEAPONS START
var reclaimedWeaponsItemIds = [
 70655,
 71215,
 71382,
 71841,
 72152,
 72389,
 72467,
 72660,
 72773,
 72952,
 73796,
 74484,
 76890,
 77078,
 77144,
 77239
];

var prebuiltCompare = function (stringList, respondChannel, bybuy, descending) {
  itemsToCompareList = JSON.parse(stringList);

  var ids = [];
  for (var i = 0; i < itemsToCompareList.length; i++) {
      ids.push(itemsToCompareList[i].id);
  }

  request(baseUrl + "v2/commerce/prices?ids=" + ids.toString(), function(error, response, result){
    linkPrices(result, respondChannel, itemsToCompareList, bybuy, descending);
  });
};

var linkPrices = function(prices, respondChannel, itemsToCompareList, bybuy, descending) {
  prices = JSON.parse(prices);

  var pricedItems = [];

  for (i = 0; i < prices.length; i++) {
    var price = prices[i];
    for (j = 0; j < itemsToCompareList.length; j++) {
      var item = itemsToCompareList[j];
      if (item.id == price.id) {
        pricedItems.push({
          id: item.id,
          name: item.name,
          sells: price.sells,
          buys: price.buys
        });
        break;
      }
    }
  }

  var sortedItems = sortByPrices(pricedItems, bybuy, descending);

  var stringToReturn = '';

  for (i = 0; i < 5; i++) {
    var item = sortedItems[i];
    var buy = convertValueToGoldString(item.buys);
    var sell = convertValueToGoldString(item.sells);

    stringToReturn += item.name + '\n\t-\tMax Buy Val:' + buy + '\t|\tMin Sell Val:' + sell + '\n';
  }

  respondChannel.send(stringToReturn);
}

var sortByPrices = function(pricedItems, bybuy, descending) {
    return pricedItems.sort(function (a, b) {
        if (a.sells == null) {
            return 1;
        }
        else if (b.sells == null) {
            return -1;
        }

        if (bybuy) {
            if (a.buys.unit_price == b.buys.unit_price) {
                return 0;
            }
            else {
                return (a.buys.unit_price < b.buys.unit_price ? 1 : -1) * (descending ? 1 : -1);
            }
        } else {
            if (a.sells.unit_price == b.sells.unit_price) {
                return 0;
            }
            else {
                return (a.sells.unit_price < b.sells.unit_price ? 1 : -1) * (descending ? 1 : -1);
            }
        }
    });
};
///////RECLAIMED WEAPONS END

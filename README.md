# gw2lunchbox discord bot

A simple discord bot intended for helpful gw2 related tools.

## Getting Started

These instructions will get you a copy of the project up and running on your own machine for your own purposes.

### Prerequisites

What things you need to install the software and how to install them

* access to your desired host server's terminal

* if you're running locally on a Windows machine i would suggest [GitBash](https://git-scm.com/downloads)

* node.js installed on your desired host server

* a discord bot [App User Token](https://discordapp.com/developers/applications/me)

* a discord server with permissions that do not restrict bots, and for this particular bot, one that allows Text-To-Speech

### Installing & Running

1) clone/download the repository to your desired folder

2) replace the line in gw2lunchbox.js that has "<YOUR DISCORD BOT USER TOKEN GOES HERE>" with your discord bot App User Token, should look something like this

```
bot.login('Mzk2OTIzNjY5Njg0OTQ0ODk2.DSx3Rw.PCZRvEroD2vWM6kKazqMLhLuJAo');
```

3) [invite your app bot to your server])(https://discordapi.com/permissions.html)

4) navigate to the folder and start the node server

```
node .
```

### Commands

```
[]help - gets all commands sent to you in a private message
[]ping - will respond to the user to confirm the bot is running
[]ttstest - sends a short test message to the server with TTS enabled
[]dhuumtimer start - starts the timer for dhuum, all responses will be sent with text and as TTS. for best results change the name of the bot on your sever to "." so that the TTS responds with " says: <actual response>"... having the full name of the bot repeated every time may not be ideal
[]dhuumtimer stop - stops the timer for dhuum
[]dhuumtimer setorb1 - set the name of "Orb 1" to whatever text you want to be read
[]dhuumtimer setorb2 - set the name of "Orb 2" to whatever text you want to be read
[]dhuumtimer setorb3 - set the name of "Orb 3" to whatever text you want to be read
[]dhuumtimer voice - toggles the Text-To-Speech feature of the dhuum timer
```

## Developer Notes

### Built With

* [Discord.js](https://discord.js.org/#/) - The base framework used to interact with the discord server

### Contributing

if this thing takes off i'll look more into a branching and approved contributors process

### Authors

* **lunchbox** - [gw2luncbhox](https://bitbucket.org/gw2lunchbox/)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.
